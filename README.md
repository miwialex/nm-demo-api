# README #

### What is this repository for? ###

This is a demo Node.js express API. the API is used to get movie genres, and to search movies using the themoviedb.org API.

I know I didn't need to write my own API, but I wanted to to showcase my skills in server side Node.js seeing how the coding exercise talked about Node.js services.

### How do I get set up? ###

1. clone the repo.
1. run `npm install` to install dependencies
1. run `npm run dev` to start the server in development mode