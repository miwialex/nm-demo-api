const express = require('express');
const bodyParser = require('body-parser');
const ev = require('express-validation');

const movieController = require('./controllers/movieController');

const app = express();

app.use(bodyParser.json());

app.use('/api/v1/movie', movieController);

app.use(function(err, req, res, next) {
	if (err instanceof ev.ValidationError) return res.status(err.status).json(err);
	next();
});

const port = process.env.PORT || 1337;

app.listen(port, function() {
	console.log(`Example app listening on port ${port}`);
});
