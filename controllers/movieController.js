const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const movieValidator = require('../validation/movieValidator');
const movieDBService = require('../services/movieDBService');


router.get('/search', validate(movieValidator.search), (req, res) => {
	movieDBService.search(req.query.query).then(results => {
		res.json({ results });
	}).catch(err => {
		res.status(500).json(err.message);
	});
});

router.get('/genres', (req, res) => {
	movieDBService.getGenres().then(results => {
		res.json(results);
	}).catch(err => {
		res.status(500).json(err.message);
	});
});

module.exports = router;