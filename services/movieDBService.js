const request = require('superagent');
const config = require('config');

const { baseUrl, apiKey } = config;

module.exports = {
	search(query) {
		return request
			.get(`${baseUrl}/search/movie`)
			.query({ query, api_key: apiKey })
			.then(res => {
				const json = JSON.parse(res.text);
				const results = json.results;
				const movies = results.map(result => {
					result.poster_path = result.poster_path ? `http://image.tmdb.org/t/p/w185/${result.poster_path}` : null;
					return result;
				});
				return movies;
			});
	},

	getGenres() {
		return request
			.get(`${baseUrl}/genre/movie/list`)
			.query({ api_key: apiKey })
			.then(res => JSON.parse(res.text));
	}
};